# super-kadr32
Site uses PHP framework Laravel 5.0

## Functional:
<ul>
<li>Social auth - VK, Facebook, Twitter, Odnoklassniki</li>
<li>Payment ROBOKASSA</li>
<li>Simple admin panel</li>
<li>Upload image with plugin CROPPIC</li>
<li>Generation playlist</li>
</ul>
